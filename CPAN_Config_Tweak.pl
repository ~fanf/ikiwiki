#!/usr/bin/perl

use warnings;
use strict;

use Data::Dumper;
use File::Basename;
use File::Path;
use FindBin qw($Script);

my $dir;
die "usage: $Script <dir>\n"
    unless $dir = shift and @ARGV == 0;
mkpath "$dir/bin";
mkpath "$dir/man";
mkpath "$dir/share";
symlink 'bin', "$dir/sbin";
symlink '../man', "$dir/share/man";

my $parent = dirname $dir;
my $cpan_prefs = "$parent/.cpan_prefs";
my $cpan_conf = "$parent/.cpan_conf";

undef $/;
my $config = qx{cpan -J};
eval $config;

$CPAN::Config->{makepl_arg} = "PREFIX= INSTALL_BASE=$dir";
$CPAN::Config->{mbuildpl_arg} = "--install_base $dir";
$CPAN::Config->{patches_dir} = "$parent/patches";
$CPAN::Config->{prefs_dir} = $cpan_prefs;

sub splurge {
	my $fn = shift;
	mkpath dirname $fn;
	open my $fh, '>', $fn
	    or die "$Script: open $fn: $!\n";
	print $fh @_;
	close $fh
	    or die "$Script: write $fn: $!\n";
}

$Data::Dumper::Sortkeys = 1;
splurge "$cpan_conf",
    Data::Dumper->Dump([$CPAN::Config],[qw[$CPAN::Config]]),
    "1;\n";

splurge "$cpan_prefs/prefs.dd", <<'END';
$VAR1 = {
	comment => "ImageMagick fixup",
	match => {
		distribution => "PerlMagick",
	},
	patches => [
		"PerlMagick.patch",
	    ],
};
END
